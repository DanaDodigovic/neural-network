#include "DrawingPanel.hpp"

#include <fstream>

DrawingPanel::DrawingPanel(wxWindow* parent) : wxPanel(parent) {}

void DrawingPanel::on_left_move(wxMouseEvent& event)
{
    if (is_down) {
        wxPoint current_pos = event.GetPosition();
        samples.back().push_back(current_pos);

        Refresh();
    }
}

void DrawingPanel::on_left_down(wxMouseEvent& /*event*/)
{
    is_down = true;
    samples.push_back({});
}

void DrawingPanel::on_left_up(wxMouseEvent& /*event*/)
{
    is_down = false;
}

void DrawingPanel::on_paint(wxPaintEvent& /*event*/)
{
    wxPaintDC dc(this);
    dc.SetPen(*wxWHITE_PEN);
    for (const auto& sample : samples) {
        const wxPoint* prev_point = nullptr;
        if (!sample.empty()) { prev_point = &sample.front(); }
        for (const auto& point : sample) {
            if (prev_point) { dc.DrawLine(*prev_point, point); }
            prev_point = &point;
        }
    }
}

/* Returns last written sample. */
std::vector<wxPoint> DrawingPanel::get_wx_sample() const
{
    if (samples.empty()) { return {}; }
    return samples.back();
}

void DrawingPanel::clear()
{
    samples.clear();
    Refresh();
}

// Define the event table
wxBEGIN_EVENT_TABLE(DrawingPanel, wxPanel) // clang-format off
    EVT_LEFT_DOWN(DrawingPanel::on_left_down)
    EVT_LEFT_UP(DrawingPanel::on_left_up)
    EVT_MOTION(DrawingPanel::on_left_move)
    EVT_PAINT(DrawingPanel::on_paint)
wxEND_EVENT_TABLE() // clang-format on
