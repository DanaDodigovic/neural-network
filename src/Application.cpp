#include "Application.hpp"

#include "NeuralNetwork.hpp"

#include <iostream>
#include <fstream>

bool Application::OnInit()
{
    MainFrame* frame = new MainFrame();
    frame->Show(true);

    return true;
}

bool Application::OnExceptionInMainLoop()
{
    try {
        throw; // Re-throw current exception.
    } catch (std::exception& e) {
        std::cout << "Exception has occurred in the main loop!\n"
                  << e.what() << "\nNow exiting..." << std::endl;
    } catch (...) {
        std::cout << "Unexpected exception has been thrown. Now exiting..."
                  << std::endl;
    }
    return false; // Do not ignore the exception. Exit the main loop.
}

void Application::OnUnhandledException()
{
    try {
        throw; // Re-throw current exception.
    } catch (std::exception& e) {
        std::cout << "Unhandled exception has occurred outside the main loop!\n"
                  << e.what() << "\nNow exiting..." << std::endl;
    } catch (...) {
        std::cout << "Unexpected exception has been thrown. Now exiting..."
                  << std::endl;
    }
}

wxIMPLEMENT_APP(Application);
