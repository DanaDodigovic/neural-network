#include "MenuPanel.hpp"
#include "Identifier.hpp"
#include "Transformation.hpp"

#include <fstream>

MenuPanel::MenuPanel(wxWindow* parent,
                     DrawingPanel* drawing_panel,
                     std::atomic<bool>& should_terminate_training)
    : wxPanel(parent),
      drawing_panel(drawing_panel),
      train_btn(new wxButton(this, ID::train_btn, "Train")),
      predict_btn(new wxButton(this, ID::predict_btn, "Predict")),
      clear_screen_btn(
          new wxButton(this, ID::clear_screen_btn, "Clear screen")),
      nn(config.architecture, should_terminate_training)
{
    predict_btn->Disable();

    auto sizer = new wxBoxSizer(wxHORIZONTAL);
    sizer->AddStretchSpacer(1);
    sizer->Add(train_btn, 4);
    sizer->AddStretchSpacer(1);
    sizer->Add(predict_btn, 4);
    sizer->AddStretchSpacer(1);
    sizer->Add(clear_screen_btn, 4);
    sizer->AddStretchSpacer(1);
    SetSizer(sizer);
}

namespace {
std::array<std::vector<Matrix>, 2>
create_mini_batches(const Matrix& X, const Matrix& T, std::size_t batch_size)
{
    std::array<std::vector<Matrix>, 2> XTs;
    auto& Xs = XTs[0];
    auto& Ts = XTs[1];

    Xs.resize(std::ceil(static_cast<double>(X.cols()) / batch_size));
    Ts.resize(std::ceil(static_cast<double>(T.cols()) / batch_size));

    for (std::size_t i = 0; i < Xs.size(); i++) {
        if (i == Xs.size() - 1) {
            std::size_t last_n = X.cols() - batch_size * i;
            Xs[i]              = X.block(0, i * batch_size, X.rows(), last_n);
            Ts[i]              = T.block(0, i * batch_size, T.rows(), last_n);
        } else {
            Xs[i] = X.block(0, i * batch_size, X.rows(), batch_size);
            Ts[i] = T.block(0, i * batch_size, T.rows(), batch_size);
        }
    }

    return XTs;
}
}

void MenuPanel::on_train_btn_pressed(wxCommandEvent& /*event*/)
{
    // Disable buttons.
    train_btn->Disable();
    predict_btn->Disable();

    train_future = std::async(std::launch::async, [this]() {
        std::ifstream ifs("../res/samples.txt");

        Matrix X(config.n_inputs, config.n_samples);
        Matrix T(config.n_outputs, config.n_samples);

        for (std::size_t i = 0; i < config.n_samples; i++) {
            double x;
            for (std::size_t j = 0; j < config.n_inputs; ++j) {
                ifs >> x;
                X(j, i) = x;
            }
            double t;
            for (std::size_t j = 0; j < config.n_outputs; ++j) {
                ifs >> t;
                T(j, i) = t;
            }
        }
        auto [Xs, Ts] = create_mini_batches(X, T, config.batch_size);

        nn.train(Xs, Ts, config.eta, config.n_iters, config.epsilon);
        wxTheApp->GetTopWindow()->GetEventHandler()->CallAfter(
            [this]() { on_train_finished(); });
    });
}

void MenuPanel::on_predict_btn_pressed(wxCommandEvent& /*event*/)
{
    auto wx_sample = drawing_panel->get_wx_sample();
    if (wx_sample.empty()) { // Check if nothing has been drawn.
        std::cout << "Draw a new sample, please!\n";
        return;
    }

    auto transformed_sample =
        Transformation::transform_sample(wx_sample, config.m);

    Matrix X_t(config.n_inputs, 1);
    std::size_t j = 0;
    for (std::size_t i = 0; i < transformed_sample.size(); i++) {
        X_t(j, 0)     = transformed_sample[i].x;
        X_t(j + 1, 0) = transformed_sample[i].y;
        j += 2;
    }

    // Neural network should always be trained at this point since
    // trainining disables predict button and re-enables it when finished.
    assert(train_future.valid() && "Predict invoked before train.");
    train_future.wait();

    auto res = nn.predict(X_t);
    std::cout << "-------- PREDICTION -------\n"
              << "alpha: " << res(0, 0) << '\n'
              << "beta:  " << res(1, 0) << '\n'
              << "gamma: " << res(2, 0) << '\n'
              << "delta: " << res(3, 0) << '\n'
              << "etha:  " << res(4, 0) << '\n'
              << "---------------------------\n\n";
}

void MenuPanel::on_clear_screen_btn_pressed(wxCommandEvent& /*event*/)
{
    drawing_panel->clear();
}

void MenuPanel::on_train_finished()
{
    train_btn->Enable();
    predict_btn->Enable();
}

wxBEGIN_EVENT_TABLE(MenuPanel, wxPanel) // clang-format off
    EVT_BUTTON(ID::train_btn, MenuPanel::on_train_btn_pressed)
    EVT_BUTTON(ID::predict_btn, MenuPanel::on_predict_btn_pressed)
    EVT_BUTTON(ID::clear_screen_btn, MenuPanel::on_clear_screen_btn_pressed)
wxEND_EVENT_TABLE() // clang-format on
