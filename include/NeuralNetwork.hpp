#ifndef NEURAL_NETWORK_HPP
#define NEURAL_NETWORK_HPP

#include "Layer.hpp"

#include <atomic>

class NeuralNetwork {
  private:
    std::vector<Layer> layers;
    FinalLayer final_layer;
    std::atomic<bool>& should_terminate_training;

  public:
    explicit NeuralNetwork(const std::vector<unsigned>& architecture,
                           std::atomic<bool>& should_terminate_training);

    void train(const std::vector<Matrix>& Xs,
               const std::vector<Matrix>& Ts,
               double eta,
               std::size_t n_iters,
               std::size_t epsilon);
    Matrix predict(const Matrix& X);
    void set_architecture(const std::vector<unsigned>& architecture);

  private:
    void forward_pass(const Matrix& X) noexcept;
    void backward_pass(const Matrix& T, double eta) noexcept;
};

#endif
