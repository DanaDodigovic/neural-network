#ifndef IDENTIFIER_HPP
#define IDENTIFIER_HPP

namespace ID {
enum : int { train_btn, predict_btn, clear_screen_btn };
}

#endif
