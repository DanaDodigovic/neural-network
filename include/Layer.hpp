#ifndef LAYER_HPP
#define LAYER_HPP

#include "Algebra.hpp"

class Layer {
  private:
    Matrix W;
    Matrix Y;

  public:
    Layer(unsigned n_neurons, unsigned n_next_layer_neurons) noexcept;

    Matrix backward_pass(const Matrix& next_delta, double eta) noexcept;
    Matrix forward_pass(const Matrix& previous_W,
                        const Matrix& previous_Y) noexcept;

    const Matrix& get_W() const noexcept;
    const Matrix& get_Y() const noexcept;
    Matrix get_dummy_Y() const noexcept;

    void set_Y(Matrix Y) noexcept;
    Matrix& get_W() noexcept;

  private:
    Matrix calculate_delta(const Matrix& next_delta) const noexcept;
    void update_weights(const Matrix& next_delta, double eta) noexcept;
    Matrix get_W_without_bias() const noexcept;
};

class FinalLayer {
  public:
    Matrix Y;

  public:
    Matrix backward_pass(const Matrix& T) const noexcept;
    const Matrix& forward_pass(const Matrix& previous_W,
                               const Matrix& previous_Y) noexcept;

  private:
    Matrix calculate_delta(const Matrix& T) const noexcept;
};

#endif
