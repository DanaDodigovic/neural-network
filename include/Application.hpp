#ifndef APPLICATION_HPP
#define APPLICATION_HPP

#undef __GXX_ABI_VERSION
#define __GXX_ABI_VERSION 1013

#include "MainFrame.hpp"

class Application : public wxApp {
  private:
    MainFrame* main_frame = nullptr;

  public:
    bool OnInit() override;
    bool OnExceptionInMainLoop() override;
    void OnUnhandledException() override;
};

wxDECLARE_APP(Application);

#endif
