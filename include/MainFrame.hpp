#ifndef MAIN_FRAME_HPP
#define MAIN_FRAME_HPP

#include "DrawingPanel.hpp"
#include "MenuPanel.hpp"

class MainFrame : public wxFrame {
  private:
    DrawingPanel* drawing_panel;
    MenuPanel* menu_panel;
    std::atomic<bool> should_terminate_training = false;

  public:
    MainFrame();
    ~MainFrame() override = default;

  private:
    void on_close_event(wxCloseEvent& event);

    wxDECLARE_EVENT_TABLE();
};

#endif
